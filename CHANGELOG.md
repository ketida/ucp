# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="2.0.5"></a>
## [2.0.5](https://gitlab.coko.foundation/editoria/ucp/compare/v2.0.4...v2.0.5) (2019-11-06)


### Bug Fixes

* **app:** wax and exporter fixes ([1a4dcbb](https://gitlab.coko.foundation/editoria/ucp/commit/1a4dcbb))



<a name="2.0.4"></a>
## [2.0.4](https://gitlab.coko.foundation/editoria/ucp/compare/v2.0.3...v2.0.4) (2019-10-30)


### Bug Fixes

* **app:** new wax fix in comments ([a0846c5](https://gitlab.coko.foundation/editoria/ucp/commit/a0846c5))



<a name="2.0.3"></a>
## [2.0.3](https://gitlab.coko.foundation/editoria/ucp/compare/v2.0.2...v2.0.3) (2019-10-25)


### Bug Fixes

* **app:** dashboard refetching ([358b703](https://gitlab.coko.foundation/editoria/ucp/commit/358b703))



<a name="2.0.2"></a>
## [2.0.2](https://gitlab.coko.foundation/editoria/ucp/compare/v2.0.1...v2.0.2) (2019-10-25)


### Bug Fixes

* **app:** jobs payload and new wax ([08df2c8](https://gitlab.coko.foundation/editoria/ucp/commit/08df2c8))



<a name="2.0.1"></a>
## [2.0.1](https://gitlab.coko.foundation/editoria/ucp/compare/v1.1.1...v2.0.1) (2019-10-23)


### Bug Fixes

* **app:** jobs added, dynamic port language tools ([9ed4717](https://gitlab.coko.foundation/editoria/ucp/commit/9ed4717))



<a name="2.0.0"></a>
# 2.0.0 (2019-10-18)


### Bug Fixes

* **webpack:** app did not load on production webpack fixes ([5b2223a](https://gitlab.coko.foundation/editoria/ucp/commit/5b2223a))
* webpack fix for apollo upload client ([1877ca3](https://gitlab.coko.foundation/editoria/ucp/commit/1877ca3))
* **add:** package lock ([ecf8fb2](https://gitlab.coko.foundation/editoria/ucp/commit/ecf8fb2))
* **app:** fix for JWT ttl ([914c5f6](https://gitlab.coko.foundation/editoria/ucp/commit/914c5f6))
* **app:** fix for signup issue ([33db79b](https://gitlab.coko.foundation/editoria/ucp/commit/33db79b))
* **app:** fix seed script errors ([27c6988](https://gitlab.coko.foundation/editoria/ucp/commit/27c6988))
* **app:** more fixes ([3cb1e48](https://gitlab.coko.foundation/editoria/ucp/commit/3cb1e48))
* **app:** no users delete ([452ab5f](https://gitlab.coko.foundation/editoria/ucp/commit/452ab5f))
* **app:** seed script fixed ([7bba722](https://gitlab.coko.foundation/editoria/ucp/commit/7bba722))
* **app:** upload fix, signup fix, unlock by admin modal ([2fa5a42](https://gitlab.coko.foundation/editoria/ucp/commit/2fa5a42))
* **app:** upload word fix and wax fix ([5dd1e57](https://gitlab.coko.foundation/editoria/ucp/commit/5dd1e57))
* **app:** various fixes ([8b8d833](https://gitlab.coko.foundation/editoria/ucp/commit/8b8d833))
* **authorize:** improve performance with Loaders ([e95c164](https://gitlab.coko.foundation/editoria/ucp/commit/e95c164))
* **authosme:** fragment to book changes to names ([b6baeb8](https://gitlab.coko.foundation/editoria/ucp/commit/b6baeb8))
* **authsome:** copy editor rules ([ce4288a](https://gitlab.coko.foundation/editoria/ucp/commit/ce4288a))
* **authsome:** fix rules ([fc3fe4e](https://gitlab.coko.foundation/editoria/ucp/commit/fc3fe4e))
* **authsome:** fix rules for author ([75eed4e](https://gitlab.coko.foundation/editoria/ucp/commit/75eed4e))
* **authsome:** fix update Book states ([a5b2c24](https://gitlab.coko.foundation/editoria/ucp/commit/a5b2c24))
* **authsome:** update team and temamanger add ([d260bb7](https://gitlab.coko.foundation/editoria/ucp/commit/d260bb7))
* **authsome:** use loaders ([5f59a88](https://gitlab.coko.foundation/editoria/ucp/commit/5f59a88))
* **componenttypes:** bookbuilder ([b8f07f7](https://gitlab.coko.foundation/editoria/ucp/commit/b8f07f7))
* **componenttypes:** tests write ([f3236cb](https://gitlab.coko.foundation/editoria/ucp/commit/f3236cb))
* **config:** update config bookbuilder script ([e4b98cd](https://gitlab.coko.foundation/editoria/ucp/commit/e4b98cd))
* **customtags:** add menu to wax ([a41cf0d](https://gitlab.coko.foundation/editoria/ucp/commit/a41cf0d))
* **epub:** add epub backend ([6db4c6c](https://gitlab.coko.foundation/editoria/ucp/commit/6db4c6c))
* **globalporduction:** change rule during read of the book ([8f58b55](https://gitlab.coko.foundation/editoria/ucp/commit/8f58b55))
* **globalteams:** change route component to load ([6ad8ecd](https://gitlab.coko.foundation/editoria/ucp/commit/6ad8ecd))
* **ink:** integration update 1.2.9 xsweet-job ([572ba8c](https://gitlab.coko.foundation/editoria/ucp/commit/572ba8c))
* **ink:** remove uneeded dependencies load job-xsweet component ([4d6d57a](https://gitlab.coko.foundation/editoria/ucp/commit/4d6d57a))
* **languagetool:** change docker image ([6f21d80](https://gitlab.coko.foundation/editoria/ucp/commit/6f21d80))
* **languagetool:** change docker image ([afe9c9c](https://gitlab.coko.foundation/editoria/ucp/commit/afe9c9c))
* **login:** styling the form for editoria ([cf9abaf](https://gitlab.coko.foundation/editoria/ucp/commit/cf9abaf))
* **login:** styling the form for editoria ([954bebf](https://gitlab.coko.foundation/editoria/ucp/commit/954bebf))
* **package:** package json remove subnstace ([d9360c8](https://gitlab.coko.foundation/editoria/ucp/commit/d9360c8))
* **seed:** fix global teams script ([cf7e4b1](https://gitlab.coko.foundation/editoria/ucp/commit/cf7e4b1))
* **xsweet:** fix docker compose with wait ([e01f063](https://gitlab.coko.foundation/editoria/ucp/commit/e01f063))


### Features

* **authsome:** make use of dataloader for user ([1f296c3](https://gitlab.coko.foundation/editoria/ucp/commit/1f296c3))
* add new api and data models ([63dce36](https://gitlab.coko.foundation/editoria/ucp/commit/63dce36))
* **app:** template manager feature ([a01456d](https://gitlab.coko.foundation/editoria/ucp/commit/a01456d))
* **applicationmanager:** bookbuilder read components types test ([8a9649e](https://gitlab.coko.foundation/editoria/ucp/commit/8a9649e))
* **applicationparameter:** get config for bookbuilder from database ([f340d32](https://gitlab.coko.foundation/editoria/ucp/commit/f340d32))
* **authsome:** migration of the authsome ([ba7df53](https://gitlab.coko.foundation/editoria/ucp/commit/ba7df53))
* **languagetool:** add language tool to docker ([1df0f39](https://gitlab.coko.foundation/editoria/ucp/commit/1df0f39))
* **login:** load logo at the login screen ([893b2f2](https://gitlab.coko.foundation/editoria/ucp/commit/893b2f2))
* **login:** update login components from editoria ([176d75a](https://gitlab.coko.foundation/editoria/ucp/commit/176d75a))
* **login:** update login components from editoria ([f4da89f](https://gitlab.coko.foundation/editoria/ucp/commit/f4da89f))
* **pagedstyle:** change route of paged page ([5a21a7e](https://gitlab.coko.foundation/editoria/ucp/commit/5a21a7e))
* **seedapplicationparameter:** seed database with config for app ([5016e7c](https://gitlab.coko.foundation/editoria/ucp/commit/5016e7c))
* **tags:** load module tags ([5966a6a](https://gitlab.coko.foundation/editoria/ucp/commit/5966a6a))
* **theme:** add support of coko theme ([c5b2ff1](https://gitlab.coko.foundation/editoria/ucp/commit/c5b2ff1))
* **theme:** add support of coko theme ([6df7f98](https://gitlab.coko.foundation/editoria/ucp/commit/6df7f98))
* **userprofile:** add user profile ([af56385](https://gitlab.coko.foundation/editoria/ucp/commit/af56385))
* substance upgrade ([259aae5](https://gitlab.coko.foundation/editoria/ucp/commit/259aae5))
* upgraded substance ([68782e7](https://gitlab.coko.foundation/editoria/ucp/commit/68782e7))
* **wax:** change wax tools menu for ucp ([6f91909](https://gitlab.coko.foundation/editoria/ucp/commit/6f91909))
* **xsweet:** add xsweet docker ([1994dd0](https://gitlab.coko.foundation/editoria/ucp/commit/1994dd0))



<a name="1.1.1"></a>
## [1.1.1](https://gitlab.coko.foundation/editoria/ucp/compare/v1.1.0...v1.1.1) (2018-12-19)



<a name="1.1.0"></a>
# 1.1.0 (2018-11-20)


### Features

* conventional commits added ([b98b958](https://gitlab.coko.foundation/editoria/ucp/commit/b98b958))



# Editoria  

## 1.3.0

#### Wax
* Search / find and replace
* Notes pane is hidden if there are no notes in the document
* Note callouts work with track changes
* Track changes previous / next navigation
* Copy / cut and paste works with track changes
* Indicator for how many items exist in a collapsed comment discussion
* Performance improvements
* Find and Replace a single match and undo throws an Error
* Undo / Redo Notes on certain Occasions throws an Error
* Opening an Empty Chapter Save is Enabled and modal is triggered if you try to go back without any change in the Editor
* if Track Changes is on, user cannot remove an image
* Remove additions on cut operation in track changes if done from the same user
* With track Changes on if you accept a deleted note and undo throws an error
* With track Changes on if you delete a whole paragraph and undo throws an error
* Navigate Through Notes with left and right arrow
* Toggle on /off Native Spell Checker
* Full screen mode
* Track Spaces
* Image Captions
* Change note icon in the toolbar
* Add keyboard shortcuts for accept & reject track changes
* Small Caps

#### Maintenance
* Switch to using yarn instead of npm by default
* React 16
* Upgrade to latest pubsweet client
* Clean up component prop types and refs
* Switch to Postgres
* Docker container for the DB provided via yarn start:services

## 1.2.0  

* Upgrade to latest Pubsweet server, client, and components  
* Introduce password reset  

#### Book Builder  
* Preview exported book in Vivliostyle Viewer  
* Chapters keep track of their own numbering (independently from parts) in the Body  
* Renaming of fragments has been removed in favour of using the title tag in Wax  
* Drag and drop fixes  

#### Wax Editor  
* Can now style multiple blocks/paragraphs at once, including lists  
* Keyboard shortcuts  
    * Turn track changes on/off => ctrl/cmd+y  
    * Hide / show tracked changes => ctrl/cmd+shift+y  
    * Comments => ctrl/cmd+m  
    * Save => ctrl/cmd+s  
* New Title style (which will also rename the fragment in the book builder)  
* New Subtitle style  
* New Bibliography Entry style  
* Revamped track changes UI: Tools are now in the toolbar instead of inline, and line height in the document has been reduced  
* Surface doesn't lose focus any more unless the user clicks outside of Wax  
* Paragraph indentation  


## 1.1.4  

#### Wax Editor  
* Fix for unsaved changes warning  


## 1.1.3  

* New design for the book builder, the dashboard and the theme  

#### Dashboard  
* Renamed 'remove' button to 'delete' for consistency with the book builder  
* Double clicking on a book will take you to the book builder for that book, instead of opening the renaming interface  
* The position of 'Edit' and 'Rename' actions have been swapped ('Edit' now comes first)  
* Books now appear in alphabetical order  

#### Book Builder  
* Fixed issue with fragments disappearing when uploading multiple files  
* Renamed 'Front Matter' and 'Back Matter' to 'Frontmatter' and 'Backmatter'  

#### Wax Editor  
* Introduce layouts  
* Accept configuration options (`layout` and `lockWhenEditing`)  
* It is now broken down into separate modules for better separation of concerns
    * Pubsweet integration  
    * React integration  
    * Core    
* Diacritics work within notes  
